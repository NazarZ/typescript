import { FightersDetails } from "../fighterDetails";
import { showFighterDetailsModal } from "./fighterDetails";
import { showModal } from "./modal";

export function showWinnerModal(fighter: FightersDetails) {
  // show winner name and image
  showFighterDetailsModal(fighter, "Winner");
}
