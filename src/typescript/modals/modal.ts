import { createElement } from "../helpers/domHelper";

export function showModal(title: string, bodyElement: HTMLDivElement) {
  const root = getModalContainer();
  const modal = createModal(title, bodyElement);

  root.append(modal);
}

function getModalContainer(): HTMLElement {
  return document.getElementById("root");
}

function createModal(title: string, bodyElement: HTMLDivElement) {
  const layer = createElement({
    tagName: "div",
    className: "modal-layer",
  }) as HTMLDivElement;
  const modalContainer = createElement({
    tagName: "div",
    className: "modal-root",
  }) as HTMLDivElement;
  const header = createHeader(title);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string): HTMLDivElement {
  const headerElement = createElement({
    tagName: "div",
    className: "modal-header",
  }) as HTMLDivElement;
  const titleElement = createElement({
    tagName: "span",
    className: "",
  }) as HTMLSpanElement;
  const closeButton = createElement({
    tagName: "div",
    className: "close-btn",
  }) as HTMLSpanElement;

  titleElement.innerText = title;
  closeButton.innerText = "×";
  closeButton.addEventListener("click", hideModal);
  headerElement.append(title, closeButton);

  return headerElement;
}

function hideModal(event: Event): void {
  const modal = document.getElementsByClassName("modal-layer")[0];
  modal?.remove();
}
