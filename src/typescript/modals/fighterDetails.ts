import { FightersDetails } from "../fighterDetails";
import { createElement } from "../helpers/domHelper";
import { showModal } from "./modal";

export function showFighterDetailsModal(
  fighter: FightersDetails,
  titl: string = "Fighter info"
): void {
  const title: string = titl;
  const bodyElement = createFighterDetails(fighter);
  showModal(title, bodyElement);
}

function createFighterDetails(fighter: FightersDetails): HTMLDivElement {
  const name: string = fighter.name;

  const fighterDetails = createElement({
    tagName: "div",
    className: "modal-body",
  }) as HTMLDivElement;
  const nameElement = createElement({
    tagName: "span",
    className: "fighter-name",
  }) as HTMLSpanElement;

  const attackElement = createElement({
    tagName: "span",
    className: "fighter-name",
  }) as HTMLSpanElement;
  const defenseElement = createElement({
    tagName: "span",
    className: "fighter-name",
  }) as HTMLSpanElement;
  const healthElement = createElement({
    tagName: "span",
    className: "fighter-name",
  }) as HTMLSpanElement;
  const attributes = { src: fighter.source };
  const imgElement = createElement({
    tagName: "img",
    className: "fighter-image",
    attributes,
  }) as HTMLImageElement;

  // show fighter name, attack, defense, health, image

  nameElement.innerText = name;
  attackElement.innerText = fighter.attack.toString();
  defenseElement.innerText = fighter.defense.toString();
  defenseElement.innerText = fighter.health.toString();

  fighterDetails.append(nameElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(imgElement);

  return fighterDetails;
}
