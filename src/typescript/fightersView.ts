import { createFighter } from "./fighterView";
import { showFighterDetailsModal } from "./modals/fighterDetails";
import { createElement } from "./helpers/domHelper";
import { Fight } from "./fight";
import { showWinnerModal } from "./modals/winner";
import { Fighter } from "./fighter";
import { getFighters, getFighterDetails } from "./services/fightersService";
import { FightersDetails } from "./fighterDetails";

const fight = new Fight();

export function createFighters(fighters: Array<Fighter>) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map((fighter) =>
    createFighter(fighter, showFighterDetails, selectFighterForBattle)
  );
  const fightersContainer = createElement({
    tagName: "div",
    className: "fighters",
  });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map<any, Fighter>();

async function showFighterDetails(event: Event, fighter: Fighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(
  fighterId: string
): Promise<FightersDetails> {
  // get fighter form fightersDetailsCache or use getFighterDetails function
  return getFighterDetails(fighterId);
}

function createFightersSelector() {
  const selectedFighters = new Map<string, Fighter>();

  return async function selectFighterForBattle(event: any, fighter: Fighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if (event.target.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else {
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      var first = selectedFighters.values().next().value;
      var second = selectedFighters.values().next().value;
      console.log(first);
      const winner = fight.fight(first, second);
      console.log(winner);
      showWinnerModal(winner);
    }
  };
}
