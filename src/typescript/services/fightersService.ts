import { Fighter } from "../fighter";
import { FightersDetails } from "../fighterDetails";
import { callApi } from "../helpers/apiHelper";

export async function getFighters(): Promise<Array<Fighter>> {
  try {
    const endpoint = "fighters.json";
    const apiResult = await callApi(endpoint, "GET");

    return apiResult as Array<Fighter>;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string): Promise<FightersDetails> {
  // endpoint - `details/fighter/${id}.json`;
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, "GET");

    return apiResult as FightersDetails;
  } catch (error) {
    throw error;
  }
}
