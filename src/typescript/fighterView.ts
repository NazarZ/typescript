import { createElement } from "./helpers/domHelper";

export function createFighter(
  fighter: any,
  handleClick: any,
  selectFighter: any
) {
  const { name, source } = fighter;
  const nameElement = createName(name);
  const imageElement = createImage(source);
  const checkboxElement = createCheckbox(source);
  const fighterContainer = createElement({
    tagName: "div",
    className: "fighter",
  });

  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev: any) => ev.stopPropagation();
  const onCheckboxClick = (ev: any) => selectFighter(ev, fighter);
  const onFighterClick = (ev: any) => handleClick(ev, fighter);

  fighterContainer.addEventListener("click", onFighterClick, false);
  checkboxElement.addEventListener("change", onCheckboxClick, false);
  checkboxElement.addEventListener("click", preventCheckboxClick, false);

  return fighterContainer;
}

function createName(name: any) {
  const nameElement = createElement({ tagName: "span", className: "name" });
  nameElement.innerText = name;

  return nameElement;
}

function createImage(source: any): HTMLImageElement {
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: "img",
    className: "fighter-image",
    attributes,
  }) as HTMLImageElement;

  return imgElement;
}

function createCheckbox(source: any) {
  const label = createElement({
    tagName: "label",
    className: "custom-checkbox",
  });
  const span = createElement({ tagName: "span", className: "checkmark" });
  const attributes = { type: "checkbox" };
  const checkboxElement = createElement({
    tagName: "input",
    className: "",
    attributes,
  }) as HTMLInputElement;
  label.append(checkboxElement, span);
  return label;
}
