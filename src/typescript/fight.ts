import { FightersDetails } from "./fighterDetails";

export class Fight {
  fight(
    firstFighter: FightersDetails,
    secondFighter: FightersDetails
  ): FightersDetails {
    let winner: FightersDetails | null = null;
    while (winner == null) {
      secondFighter.health -= this.getDamage(firstFighter, secondFighter);
      if (secondFighter.health <= 0) {
        winner = firstFighter;
      }
      firstFighter.health -= this.getDamage(secondFighter, firstFighter);
      if (firstFighter.health <= 0) {
        winner = secondFighter;
      }
    }
    return winner;
  }

  getDamage(attacker: FightersDetails, enemy: FightersDetails): number {
    // damage = hit - block
    // return damage
    return this.getHitPower(attacker) - this.getBlockPower(enemy);
  }

  getHitPower(fighter: FightersDetails) {
    // return hit power
    return fighter.attack * Math.floor(Math.random() * 2) + 1;
  }

  getBlockPower(fighter: FightersDetails) {
    // return block power
    return fighter.defense * Math.floor(Math.random() * 2) + 1;
  }
}
