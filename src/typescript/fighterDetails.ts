export class FightersDetails {
  _id: string;
  name: string;
  source: string;
  health: number;
  attack: number;
  defense: number;
}
